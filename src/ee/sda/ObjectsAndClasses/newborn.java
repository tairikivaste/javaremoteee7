package ee.sda.ObjectsAndClasses;

public class newborn {

    String gender;
    String length;
    String weight;
    private int id;
    private parents parents;

    public newborn(String gender, String length, String weight, double id) {
        this.gender = gender;
        this.length = length;
        this.weight = weight;
        this.id = (int) id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = (int) id;
    }

    public ee.sda.ObjectsAndClasses.parents getParents() {
        return parents;
    }

    public void setParents(ee.sda.ObjectsAndClasses.parents parents) {
        this.parents = parents;
    }
}
