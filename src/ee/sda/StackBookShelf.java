package ee.sda;

import java.util.Stack;

public class StackBookShelf {

    public static void main(String[] args) {

        Stack<String> bookShelf= new Stack<>();

        bookShelf.push("Book1");
        bookShelf.push("Book2");
        bookShelf.push("Book3");
        bookShelf.push("Book4");
        System.out.println("Stack: " + bookShelf);

        bookShelf.pop();
        System.out.println("Stack: " + bookShelf);

    }
}

