package ee.sda.InnerClass;

public class StrollerExample {

    public static void main(String[] args) {

        Stroller brEra = new Stroller();

        brEra.setPrice(619);

        Stroller.looks looks = brEra.new looks();
        looks.frameColor = "black;";
        looks.materialColor = "black";

        Stroller.size size = brEra.new size();
        size.height = 113;
        size.width = 62;
        size.length = 80;
        size.weight = 12.25;

        Stroller.wheels wheels = brEra.new wheels();
        wheels.amount = 4;
        wheels.backWheelSize = 11.5;
        wheels.frontWheelSize = 8.5;

        Stroller.loadCapacity loadCapacity = brEra.new loadCapacity();
        loadCapacity.maxCapacity = 25;

    }
}
