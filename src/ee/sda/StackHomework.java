package ee.sda;

public class StackHomework {

    private int[] array;
    private int booksOnTopOfEachOther;
    private int max;


    public StackHomework(int max) {
        this.array = new int [max];
        this.booksOnTopOfEachOther = 0;
        this.max = max;


    }

    public void addNewBook (String newBook){

        if(booksOnTopOfEachOther >= max){
            System.out.println("Sorry, the shelf is full!");
        } else{
            array[booksOnTopOfEachOther] = newBook;
            booksOnTopOfEachOther ++;
        }
    }

    public int removeTheBook() {

        if(booksOnTopOfEachOther == 0){
            System.out.println("Sorry, there is no books to take");

            return 0;
        } else {
            booksOnTopOfEachOther--;

            return array[booksOnTopOfEachOther];
        }
    }

    public static void main(String[] args) {

        StackHomework bookShelf = new StackHomework(15);

        bookShelf.addNewBook("Book1");
        bookShelf.addNewBook("Book2");
        bookShelf.addNewBook("Book3");
        bookShelf.addNewBook("Book4");
        bookShelf.addNewBook("Book5");
        bookShelf.addNewBook("Book6");

        System.out.println("The book taken from the top: " +bookShelf.removeTheBook());
        System.out.println("Another book taken : " +bookShelf.removeTheBook());

    }
}
