package ee.sda.ObjectsAndClasses;

public class parents {

    String mother;
    String father;
    private String surname;
    String city;
    private int mothersID;

    public parents(String surname) {
        this.surname = surname;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getMothersID() {
        return mothersID;
    }

    public void setMothersID(double mothersID) {
        this.mothersID = (int) mothersID;
    }
}
