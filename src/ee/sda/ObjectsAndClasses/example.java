package ee.sda.ObjectsAndClasses;

public class example {

    public static void main(String[] args) {

        newborn newborn1 = new newborn("girl", "49cm",
                "3,2kg", 123456);

        parents parents1 = new parents("Kask");

        parents1.setMother("Anna");
        parents1.setFather("Igor");
        parents1.setCity("Tartu");
        parents1.setMothersID(234567);

        newborn1.setParents(parents1);

        System.out.println( "Newborn " + newborn1.getId() + " belongs to family "
        + parents1.getSurname());
    }
}
