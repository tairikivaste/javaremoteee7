package ee.sda;

import java.util.Arrays;

public class SeparateAsAndBs {

    public static void main(String[] args) {

        char[] input = {'a' , 'b' , 'a' , 'b' , 'b' , 'b' , 'b' , 'a' , 'a' , 'a' ,
                'a' , 'b' , 'b' , 'b'};
        Arrays.sort(input);
        System.out.println(Arrays.toString(input));

    }
}

