package ee.sda.InnerClass;

public class Stroller {

    double price;

    class looks {
        String materialColor;
        String frameColor;
    }

    class wheels {
        int amount;
        double backWheelSize;
        double frontWheelSize;
    }
    class size {
        double height;
        double width;
        double length;
        double weight;
    }

    class loadCapacity {
        double maxCapacity;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
